﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(VGC.Startup))]
namespace VGC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
